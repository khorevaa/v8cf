/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.parsers.EmbeddedSectionParser;
import ru.shmalevoz.v8cf.metadata.MDElement;

/**
 *
 * @author shmalevoz
 */
public class Commands extends EmbeddedSectionParser {
	
	// Команды объекта
	private final String NAME_PATH;
	private final String UUID_PATH;
	private static final String UUID_POSTFIX = ".2";
	private final String UUID;
	// = "d5207c64-11d5-4d46-bba2-55b7b07ff4eb";
	
	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	
	public Commands(String u) {
		super(UUID_POSTFIX, MDElement.MODULE);
		UUID = u;
		// Пути по-умолчанию. есть иссключения, например команды журнала документов
		NAME_PATH = "0/0/1/3/2/9/2";
		UUID_PATH = "0/0/1/3/1/1";
	}
	
	public Commands(String u, String n_p, String u_p) {
		super(UUID_POSTFIX, MDElement.MODULE);
		UUID = u;
		NAME_PATH = n_p;
		UUID_PATH = u_p;
	}

	@Override
	public String getObjectNamePath() {
		return NAME_PATH;
	}
	
	@Override
	public String getObjectUUIDPath() {
		return UUID_PATH;
	}
	
	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}

	@Override
	public boolean hasStorageEntry() {
		return true;
	}
	
}
