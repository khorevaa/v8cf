/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class BusinessProcesses extends SectionParser {
	
	//private static final String name = "БизнесПроцессы";
	// 87c988de-ecbf-413b-87b0-b9516df05e28 Реквизиты
	// a3fe6537-d787-40f7-8a06-419d2f0c1cfd Табличные части
	
	private static final String UUID = "fcd3404e-1523-48ce-9bc0-ecdb822684a1";
	private static final String PATH = "0/0/1/1/2";
	private static final String FORMS_UUID = "3f7a8120-b71a-4265-98bf-4d9bc09b7719"; // 0/0/4/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/2";
	private static final String COMMANDS_UUID = "7a3e533c-f232-40d5-a932-6a311d2480bf"; // Путь 0/0/5/0
	

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".5", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("КартаМаршрута", ".7", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".6", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".8", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID, FORMS_NAME_PATH)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
