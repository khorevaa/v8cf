/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata;

import java.io.IOException;
import ru.shmalevoz.v8cf.metadata.parsers.EntryParser;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 * Ссылка на объект метаданных
 * @author shmalevoz
 */
public class MDEntryLink {
	
	private final String name;
	private final EntryParser parser;
	private final MDEntry parent;
	private final EntryLink link;
	private final boolean is_folder;
	
	
	/**
	 * Конструктор. Используется для подчиненных секций-групп
	 * @param n Имя объекта
	 * @param e Парсер объекта
	 * @param p Родитель объекта
	 */
	public MDEntryLink(String n, EntryParser e, MDEntry p) {
		name = n;
		parser = e;
		parent = p;
		
		link = null;
		is_folder = true;
	}
	
	/**
	 * Конструктор. Используется для конечных объектов с получением имени из парсера
	 * @param p Парсер объекта
	 * @param e Родитель объекта
	 * @param l Ссылка на элемент хранилища
	 * @param is_dir Признак каталога
	 */
	public MDEntryLink(EntryParser p, MDEntry e, EntryLink l, boolean is_dir) {
		name = null;
		parser = p;
		parent = e;
		link = l;
		is_folder = is_dir;
	}
	
	/**
	 * Конструктор. Используется для именованных конечных объектов
	 * @param n Имя объекта
	 * @param p Парсер объекта
	 * @param e Родитель объекта
	 * @param l Ссылка на элемент хранилища
	 * @param is_dir Признак каталога
	 */
	public MDEntryLink(String n, EntryParser p, MDEntry e, EntryLink l, boolean is_dir) {
		name = n;
		parser = p;
		parent = e;
		link = l;
		is_folder = is_dir;
	}
	
	/**
	 * Создание объекта метаданных
	 * @return Объект метаданных
	 * @throws IOException 
	 */
	public MDEntry createEntry() throws IOException {
		
		if (link == null) {
			return new MDEntry(name, parser, parent);
		}
		if (name == null) {
			return new MDEntry(parser, parent, link, is_folder);
		}
		
		return new MDEntry(name, parser, parent, link, is_folder);
	}
	
}
